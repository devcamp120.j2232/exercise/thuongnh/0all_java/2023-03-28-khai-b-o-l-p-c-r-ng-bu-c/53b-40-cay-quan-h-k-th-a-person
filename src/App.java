import java.util.Stack;

public class App {
    public static void main(String[] args) throws Exception {
        // yc 5 tạo ra hai đối tượng con người
        Person person1 = new Person("linh", "ha noi");
        Person person2 = new Person("trang", "da nang");

        System.out.println(person1);
        System.out.println(person2);

        // yc6 tạo ra hai đối tượng học sinh
        Student student1 = new Student("thuong", "ha noi", "casio", 2012, 1000);
        Student student2 = new Student("huy", "thanh hoa", "dell", 2012, 9084);

        System.out.println(student1);
        System.out.println(student2);

        // yêu cầu 7 tạo ra 2 nhân viên

        Staff staff1 = new Staff("thuong", "ha noi", "bach khoa ha noi", 2012.0);

        Staff staff2 = new Staff("linj", "da nang", "bach khoa ha noi", 2012.0);

        System.out.println(staff1);
        System.out.println(staff2);




    }
}
