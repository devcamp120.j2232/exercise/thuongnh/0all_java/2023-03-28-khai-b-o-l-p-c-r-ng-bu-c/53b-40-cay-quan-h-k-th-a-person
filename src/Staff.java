public class Staff  extends Person{

    private String sclool;
    private double pay;




    public Staff(String name, String address, String sclool, double pay) {
        super(name, address);
        this.sclool = sclool;
        this.pay = pay;
    }




    public String getSclool() {
        return sclool;
    }




    public void setSclool(String sclool) {
        this.sclool = sclool;
    }




    public double getPay() {
        return pay;
    }




    public void setPay(double pay) {
        this.pay = pay;
    }




    @Override
    public String toString() {
        return "Staff [Person[name= " + this.getName() + ", address= " + this.getAddress() + " ], "  + ", sclool=" + sclool + ", pay=" + pay + "]";
    }


    
    

    
    
}
